package com.sampark.dsign.service;

import java.io.File;
import java.io.FilenameFilter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.sampark.dsign.constants.Constants;
import com.sampark.dsign.model.ProfileBean;
import com.sampark.dsign.model.ProfileSignatories;
import com.sampark.dsign.thread.SignDocumentThread;
import com.sampark.dsign.util.SignatureUtil;
import com.sampark.dsign.util.ZipUtility;

public class SignDocumentService {
	static Logger logger = Logger.getLogger(DocumentService.class);
	static Logger durationLogger = Logger.getLogger("DURATION");
	Long t1 = 0L;
	
	public void processDocument(ProfileBean profile, ProfileSignatories profileSign) {
		logger.info("StartTime:"+new Timestamp(System.currentTimeMillis()));
		MailService mailService = new MailService();
		
		File sourcePath = new File(profile.getInputFolder());
		String destinationPath = profile.getOutputFolder();
		
		List<File> fileList = null;
		t1 = System.currentTimeMillis();
		
		try {
			fileList = SignatureUtil.getAssortedList(sourcePath,".pdf");
		} catch (NullPointerException npx) {
			logger.info("The source folder cannot be accessed at the moment.");
			//mailService.prepareMail("error", profile.getRowId(), "The source folder cannot be accessed at the moment.");
			//configure alert
		}
		durationLogger.info("Time to fetch List of files from file system"+(System.currentTimeMillis() - t1));
		
		logger.info("Worker thread starting. Processing list of items:"+fileList.size());
		Iterator<File> fileItr = fileList.iterator();
		if(fileList!=null && fileList.size() > 0) {
			while(fileItr.hasNext()) {
				File file = fileItr.next();
				try {
					SignDocumentThread worker = new SignDocumentThread(sourcePath+File.separator+file.getName(), destinationPath+File.separator+file.getName(),profileSign);
		            worker.run();
				} catch (Exception e) {
					logger.info(e.getMessage());
					e.printStackTrace();
				}
			}
			
			//Sedning mail to the client
			Integer metaDataFilesCount = new MailToClientService().sendMail(profile.getInputFolder(), profile.getOutputFolder());
			if(metaDataFilesCount > 0) {
				Integer [] arg = {fileList.size() - Constants.Failed,Constants.Failed,fileList.size()};
		        mailService.prepareMail("info", profile.getRowId(), arg);
			} else {
				mailService.prepareMail("error", profile.getRowId(), "Metadata file missing");
			}
			
			
	        //dao.getFailedCount(profile.getRowId(), new Date());
	        
	        logger.info("Creating zip file for the processed files");
	        ZipUtility zipFiles = new ZipUtility();
			zipFiles.createTarFile(profile.getInputFolder(),"current");
			zipFiles.createTarFile(profile.getOutputFolder(),"processed");
			//zipFiles.deleteOldGZipFiles();
			
		} else {
			logger.info("Could not connect to the SAN Storage");
			//configure alert
			Integer [] arg = {fileList.size() - Constants.Failed,Constants.Failed,fileList.size()};
	        mailService.prepareMail("info", profile.getRowId(), arg);
	        
			
		}
        
	}
	
	private List<File> getList(File source) {
		//ArrayList<File> files = new ArrayList<File>(Arrays.asList(source.listFiles()));
		ArrayList<File> files = new ArrayList<File>(Arrays.asList(source.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".pdf");
			}
		})));	
		
		Collections.sort(files, new Comparator<File>() {
		    @Override
		    public int compare(File o1, File o2) {
		        Long l1 = (Long)o1.lastModified();
		        Long l2 = (Long)o2.lastModified();
		    	return l1.compareTo(l2);
		    }
		});
		if(files.size()>=10000) {
			return files.subList(0, 50);
		} else {
			return files.subList(0, files.size());
		}
	}

}
